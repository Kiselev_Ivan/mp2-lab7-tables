#include "../gtest/gtest.h"
#include "../include/TTreeTable.h"
#include "../include/TBalanceTree.h"

TEST(TTreeNode, can_create_TTNode) {
	EXPECT_NO_FATAL_FAILURE(TTreeNode node());
}

TEST(TTreeNode, can_get_tree_branch) {
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("IIsus", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("Kiselev I.", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("GOD", (PTDatValue)&i[0],&left,&right);

	EXPECT_EQ(Center.GetLeft(), &left);
	EXPECT_EQ(Center.GetRight(), &right);
}

TEST(TBalanceNode, can_created_TBN) {
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBN);
}

TEST(TTreeTable, can_create_TTT) {
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TTreeTable, can_ins_rec) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("that", (PTDatValue)&i[0]));
	table->InsRecord("thot", (PTDatValue)&i[0]);
	table->InsRecord("thut", (PTDatValue)&i[0]);
}

TEST(TTreeTable, can_find_rec) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("that", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("thot", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("thut", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("thot"), 5);
}

TEST(TBalanceTree, can_create_TBT) {
	EXPECT_NO_FATAL_FAILURE(TBalanceTree tree);
}

TEST(TBalanceTree, can_add_and_find_rec) {
	int* i = new int(1);
	i[0] = 3;
	TBalanceTree* table = new TBalanceTree();
	table->InsRecord("phat", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("jhat", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 1;
	table->InsRecord("jhot", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("jhut", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("thut", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("jhot"), 1);
}

