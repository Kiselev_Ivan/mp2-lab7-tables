#ifndef INCLUDE_TDATVALUE_H_
#define INCLUDE_TDATVALUE_H_

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue
{
public:
  virtual PTDatValue GetCopy() = 0; // �������� �����

  ~TDatValue() = default;
};

#endif  // INCLUDE_TDATVALUE_H_