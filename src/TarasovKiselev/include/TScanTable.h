#ifndef INCLUDE_TSCANTABLE_H_
#define INCLUDE_TSCANTABLE_H_

#include "TArrayTable.h"

class TScanTable : public TArrayTable
{
public:
	TScanTable(int Size) : TArrayTable(Size) { }

	// �������� ������
	virtual PTDatValue FindRecord(TKey k);
	virtual void InsRecord(TKey k, PTDatValue pVal);
	virtual void DelRecord(TKey k);
};

#endif